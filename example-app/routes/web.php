<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\CounponController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Client\CartController;
use App\Http\Controllers\Client\HomeController;
use App\Http\Controllers\Client\OrderController;
use App\Http\Controllers\Client\ProductController as ClientProductController;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\OrderController as AdminOrderController;
use App\Http\Controllers\Admin\CounponController as AdminCounponController;
use App\Http\Controllers\Admin\ProductController as AdminProductController;
use App\Http\Controllers\Admin\RoleController as AdminRoleController;
use App\Http\Controllers\Admin\UserController as AdminUserController;
use App\Http\Controllers\Admin\CategoryController as AdminCategoryController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\App;

if (App::environment('production')) {
    URL::forceScheme('https');
}

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// route client
Route::get('/', [HomeController::class, 'index'])->name('client.home');


Route::get('product/', [ClientProductController::class, 'index'])->name('client.products.index');
Route::get('product-detail/{id}', [ClientProductController::class, 'show'])->name('client.products.show');

Route::middleware('auth')->group(function(){
    Route::resource('carts', 'Client\CartController');
    Route::post('carts/add', [CartController::class, 'addToCart'])->name('client.carts.add');
    Route::get('/get-image/{size}/{product_id}', 'ProductController@getImage')->name('client.products.get-image');
    Route::get('carts', [CartController::class, 'index'])->name('client.carts.index');
    Route::post('update-quantity-product-in-cart/{cart_product_id}', [CartController::class, 'updateQuantityProduct'])->name('client.carts.update_product_quantity');
    Route::post('remove-product-in-cart/{cart_product_id}', [CartController::class, 'removeProductInCart'])->name('client.carts.remove_product');
    Route::post('apply-coupon', [CartController::class, 'applyCoupon'])->name('client.carts.apply_coupon');
    Route::get('checkout', [CartController::class, 'checkout'])->name('client.checkout.index')->middleware('user.can_checkout_cart');
    Route::post('process-checkout', [CartController::class, 'processCheckout'])->name('client.checkout.proccess')->middleware('user.can_checkout_cart');
    Route::get('list-orders', [OrderController::class, 'index'])->name('client.orders.index');
    Route::post('orders/cancel/{id}', [OrderController::class, 'cancel'])->name('client.orders.cancel');
});

// Route::resource('coupons', 'Admin\CouponController');
Auth::routes();
Route::middleware('auth')->group(function(){
    Route::resource('coupons', 'Admin\CouponController');
    Route::get('coupons', [AdminCounponController::class, 'index'])->name('coupons.index');
    Route::get('/coupons/create', [AdminCounponController::class, 'index'])->name('coupons.create');
});


Route::middleware('auth')->group(function(){
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::resource('roles', AdminRoleController::class);
    Route::get('roles/{role}', [AdminRoleController::class, 'show'])->name('roles.show');
    Route::get('roles/{role}/edit', [AdminRoleController::class, 'edit'])->name('roles.edit');
    Route::get('/roles/search', [AdminRoleController::class, 'search'])->name('roles.search');
    Route::delete('roles/{id}', [AdminRoleController::class, 'destroy'])->name('roles.destroy');

    Route::resource('users', AdminUserController::class);
    Route::get('users/{id}/show', [AdminUserController::class, 'show'])->name('users.show');
    Route::get('/users/search', [AdminUserController::class, 'search'])->name('users.search');


    Route::resource('categories', AdminCategoryController::class);

    Route::resource('products', AdminProductController::class);
    Route::get('/products', [AdminProductController::class, 'index'])->name('products.index');
    Route::get('/products/create', [AdminProductController::class, 'create'])->name('admin.products.create');
    Route::get('products/{product}/edit', [AdminProductController::class, 'edit'])->name('admin.products.edit');
    Route::get('products/{product}', [AdminProductController::class, 'show'])->name('admin.products.show');
    Route::delete('/products/{id}', [AdminProductController::class, 'destroy'])->name('admin.products.destroy');
    Route::put('/products/{id}', [AdminProductController::class, 'update'])->name('admin.products.update');

    Route::get('orders', [AdminOrderController::class, 'index'])->name('admin.orders.index');

    Route::get('product/search/', [ClientProductController::class, 'search'])->name('client.products.search');

    //   Route::resource('roles', RoleController::class);
    //   Route::prefix('roles')->controller(RoleController::class)->name('roles.')->group(function(){
    //     Route::get('/', 'index')->name('index')->middleware('role:super-admin');
    //     Route::post('/', 'store')->name('store')->middleware('role:super-admin');
    //     Route::get('/create', 'create')->name('create')->middleware('role:super-admin');
    //     Route::get('/{coupon}', 'show')->name('show')->middleware('role:super-admin');
    //     Route::put('/{coupon}', 'update')->name('update')->middleware('role:super-admin');
    //     Route::get('/{role}', 'show')->name('show')->middleware('role:super-admin');
    //     Route::delete('/{coupon}', 'destroy')->name('destroy')->middleware('role:super-admin');
    //     Route::get('/{coupon}/edit', 'edit')->name('edit')->middleware('role:super-admin');
    // });
    // Route::resource('users', UserController::class);
    // Route::prefix('users')->controller(UserController::class)->name('users.')->group(function(){
    //     Route::get('/', 'index')->name('index')->middleware('permission:show-user');
    //     Route::post('/', 'store')->name('store');
    //     Route::get('/create', 'create')->name('create')->middleware('permission:create-user');
    //     Route::get('/{user}', 'show')->name('show')->middleware('permission:show-user');
    //     Route::get('/{coupon}', 'show')->name('show')->middleware('permission:show-user');
    //     Route::put('/{coupon}', 'update')->name('update')->middleware('permission:update-user');
    //     Route::delete('/{coupon}', 'destroy')->name('destroy')->middleware('permission:delete-user');
    //     Route::get('/{coupon}/edit', 'edit')->name('edit')->middleware('permission:update-user');
    // });
    // Route::resource('categories', CategoryController::class);
    // Route::prefix('categories')->controller(CategoryController::class)->name('categories.')->group(function(){
    //     Route::get('/', 'index')->name('index')->middleware('permission:show-category');
    //     Route::post('/', 'store')->name('store')->middleware('permission:create-category');
    //     Route::get('/create', 'create')->name('create')->middleware('permission:create-category');
    //     Route::get('/{coupon}', 'show')->name('show')->middleware('permission:show-category');
    //     Route::put('/{coupon}', 'update')->name('update')->middleware('permission:update-category');
    //     Route::delete('/{coupon}', 'destroy')->name('destroy')->middleware('permission:delete-category');
    //     Route::get('/{coupon}/edit', 'edit')->name('edit')->middleware('permission:update-category');
    // });

    // Route::resource('products', ProductController::class);

    // Route::prefix('products')->controller(ProductController::class)->name('products.')->group(function(){
    //     Route::get('/', 'index')->name('index')->middleware('permission:show-product');
    //     Route::post('/', 'store')->name('store')->middleware('permission:create-product');
    //     Route::get('/create', 'create')->name('create')->middleware('permission:create-product');
    //     Route::get('/{coupon}', 'show')->name('show')->middleware('permission:show-product');
    //     Route::put('/{coupon}', 'update')->name('update')->middleware('permission:update-product');
    //     Route::delete('/{coupon}', 'destroy')->name('destroy')->middleware('permission:delete-product');
    //     Route::get('/{coupon}/edit', 'edit')->name('edit')->middleware('permission:update-product');
    // });
    // Route::prefix('coupons')->controller(CounponController::class)->name('coupons.')->group(function(){
        // Route::get('/', 'index')->name('index')->middleware('permission:show-coupon');
        // Route::post('/', 'store')->name('store')->middleware('permission:create-coupon');
        // Route::get('/create', 'create')->name('create')->middleware('permission:create-coupon');
        // Route::get('/{coupon}', 'show')->name('show')->middleware('permission:show-coupon');
        // Route::put('/{coupon}', 'update')->name('update')->middleware('permission:update-coupon');
        // Route::delete('/{coupon}', 'destroy')->name('destroy')->middleware('permission:delete-coupon');
        // Route::get('/{coupon}/edit', 'edit')->name('edit')->middleware('permission:update-coupon');
    // });    
    Route::post('update-status/{id}', [AdminOrderController::class, 'updateStatus'])->name('admin.orders.update_status')->middleware('list-order');
});


