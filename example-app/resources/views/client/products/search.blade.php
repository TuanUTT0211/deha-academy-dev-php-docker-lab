@extends('client.layouts.app')

@section('content')
    <div class="container">
        <h2>Search Results for "{{ $search }}"</h2>
        @if($products->isEmpty())
            <p>No products found.</p>
        @else
            <div class="row">
                @foreach($products as $product)
                    <div class="col-md-4 mb-3">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">{{ $product->name }}</h5>
                                <p class="card-text">{{ $product->description }}</p>
                                <a href="{{ route('client.products.show', $product->id) }}" class="btn btn-primary">View Product</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
    </div>
@endsection
