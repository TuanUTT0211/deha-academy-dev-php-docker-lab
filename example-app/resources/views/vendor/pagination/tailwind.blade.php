<style>
    .pagination-container {
        display: flex;
        justify-content: center;
        align-items: center;
        margin-top: 10px;
    }

    .pagination-link {
        display: inline-block;
        padding: 8px 16px;
        margin: 0 4px;
        font-size: 14px;
        font-weight: 500;
        color: #333;
        text-decoration: none;
        border: 1px solid #ccc;
        border-radius: 4px;
        transition: background-color 0.3s ease, color 0.3s ease, border-color 0.3s ease, transform 0.2s ease;
    }

    .pagination-link:hover {
        background-color: #f0f0f0;
        color: #007bff;
        border-color: #007bff;
        transform: translateY(-2px); 
    }

    .pagination-current {
        background-color: #007bff;
        color: #fff;
        border-color: #007bff;
    }

    .pagination-disabled {
        pointer-events: none;
        opacity: 0.5;
    }
</style>


@if ($paginator->hasPages())
    <nav role="navigation" aria-label="{{ __('Pagination Navigation') }}" class="pagination-container">
        <div>
            @if ($paginator->onFirstPage())
                <span class="pagination-link pagination-disabled">&laquo; {!! __('pagination.previous') !!}</span>
            @else
                <a href="{{ $paginator->previousPageUrl() }}" class="pagination-link">&laquo; {!! __('pagination.previous') !!}</a>
            @endif

            @foreach ($elements as $element)
                @if (is_string($element))
                    <span class="pagination-link pagination-disabled">{{ $element }}</span>
                @endif

                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <span class="pagination-link pagination-current">{{ $page }}</span>
                        @else
                            <a href="{{ $url }}" class="pagination-link">{{ $page }}</a>
                        @endif
                    @endforeach
                @endif
            @endforeach

            @if ($paginator->hasMorePages())
                <a href="{{ $paginator->nextPageUrl() }}" class="pagination-link">{!! __('pagination.next') !!} &raquo;</a>
            @else
                <span class="pagination-link pagination-disabled">{!! __('pagination.next') !!} &raquo;</span>
            @endif
        </div>
    </nav>
@endif
