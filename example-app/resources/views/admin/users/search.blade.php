@extends('admin.layouts.app')

@section('title', 'Search Users')

@section('content')
    <div class="card">
        <h1>Search Users</h1>

        <form action="{{ route('users.search') }}" method="GET">
            <div class="form-group">
                <label for="search">Search:</label>
                <input type="text" name="search" id="search" class="form-control" placeholder="Enter search">
            </div>
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
    </div>
@endsection
