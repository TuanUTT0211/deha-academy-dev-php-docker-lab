@extends('admin.layouts.app')
@section('title', 'Coupons')
@section('content')
    <div class="card">

        @if (session('message'))
            <h1 class="text-primary">{{ session('message') }}</h1>
        @endif


        <h1>
            Coupon list
        </h1>
        <div>
            <a href="{{ route('coupons.create') }}" class="btn btn-primary">Create</a>

        </div>
        <div>
            <table class="table table-hover">
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Value</th>
                    <th>Expery Date</th>

                    <th>Action</th>
                </tr>

                @foreach ($coupons as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->name }}</td>

                        <td>{{ $item->type }}</td>
                        <td>{{ $item->value }}</td>
                        <td>{{ $item->expery_date }}</td>

                        <td>
                                <a href="{{ route('coupons.edit', $item->id) }}" class="btn btn-warning">Edit</a>
                                <form action="{{ route('coupons.destroy', $item->id) }}" id="form-delete{{ $item->id }}"
                                    method="post">
                                    @csrf
                                    @method('delete')

                                </form>

                                <button class="btn btn-delete btn-danger delete-coupon" data-id="{{ $item->id }}">Delete</button>
                        </td>
                    </tr>
                @endforeach
            </table>
            {{ $coupons->links() }}
        </div>

    </div>

@endsection

@section('script')
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            // Add an event listener to all delete buttons
            document.querySelectorAll('.delete-coupon').forEach(function (button) {
                button.addEventListener('click', function () {
                    // Get the coupon ID from the data-id attribute
                    var couponId = this.getAttribute('data-id');

                    // Confirm deletion
                    if (confirm('Are you sure you want to delete this coupon?')) {
                        // Make an Ajax request
                        axios.delete('/coupons/' + couponId)
                            .then(function (response) {
                                // Remove the deleted coupon from the table
                                document.getElementById('form-delete' + couponId).submit();
                                alert('Coupon deleted successfully!');
                            })
                            .catch(function (error) {
                                console.error('Error deleting coupon:', error);
                                alert('An error occurred while deleting the coupon.');
                            });
                    }
                });
            });
        });
    </script>
@endsection


