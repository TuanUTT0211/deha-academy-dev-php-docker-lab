@extends('admin.layouts.app')

@section('title', 'Category')

@section('content')
    <div class="card">
        @if (session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif

        <h1>Category List</h1>
        <div class="mb-3">
            <a href="{{ route('categories.create') }}" class="btn btn-primary">Create</a>
        </div>

        <div>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Parent ID</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($categories as $item)
                        <tr>
                    
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->parent_id }}</td>
                            <td>
                                <a href="{{ route('categories.edit', $item->id) }}" class="btn btn-warning">Edit</a>
                                <button class="btn btn-delete btn-danger" data-id="{{ $item->id }}">Delete</button>
                                <form action="{{ route('categories.destroy', $item->id) }}" method="post"
                                    id="form-delete{{ $item->id }}" style="display: none;">
                                    @csrf
                                    @method('delete')
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4">No categories found.</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>

            {{ $categories->links() }}
        </div>
    </div>
@endsection

@section('script')
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            // Add your custom JavaScript here
        });
    </script>
@endsection
