@extends('admin.layouts.app')
@section('title', 'Create Category')
@section('content')
    <div class="card">
        <h1>Create Category</h1>

        <div>
            <form action="{{ route('categories.store') }}" method="post">
                @csrf

                <div class="input-group input-group-static mb-4">
                    <label>Name</label>
                    <input type="text" value="{{ old('name') }}" name="name" class="form-control">

                    @error('name')
                        <span class="text-danger"> {{ $message }}</span>
                    @enderror
                </div>

                <select name="parent_id" class="form-control">
                    <option value="">Select Parent Category</option>
                    @foreach ($parentCategories as $item)
                        <option value="{{ $item->id }}" {{ old('parent_id') == $item->id ? 'selected' : '' }}>
                            {{ $item->name }}
                        </option>
                    @endforeach
                </select>

                @error('parent_id')
                    <span class="text-danger">{{ $message }}</span>
                @enderror


                <button type="submit" class="btn btn-submit btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
