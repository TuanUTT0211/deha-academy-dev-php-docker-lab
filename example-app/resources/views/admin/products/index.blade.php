@extends('admin.layouts.app')
@section('title', 'Products')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="card">

        @if (session('message'))
            <h1 class="text-primary">{{ session('message') }}</h1>
        @endif

        <h1>
            Products list
        </h1>
        <div>
            <a href="{{ route('admin.products.create') }}" class="btn btn-primary">Create</a>

        </div>
        <div>
            <table class="table table-hover">
                <tr>
                    <th>#</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Sale</th>
                    <th>Action</th>
                </tr>
                @foreach ($products as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td><img src="{{ asset($item->image_path) }}" width="100px" height="100px"></td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->price }}</td>
                        <td>{{ $item->sale }}</td>
                        <td>
                            <a href="{{ route('admin.products.edit', $item->id) }}" class="btn btn-warning">Edit</a>
                            <a href="{{ route('admin.products.show', $item->id) }}" class="btn btn-warning">Show</a>
                            <form action="{{ route('admin.products.destroy', $item->id) }}" id="form-delete{{ $item->id }}" method="post">
                                @csrf
                                @method('delete')
                            </form>
                            <button class="btn btn-delete btn-danger" data-id="{{ $item->id }}">Delete</button>
                        </td>
                    </tr>
                @endforeach

        </table>
        {{ $products->links() }}
    </div>

</div>

@endsection

@section('script')
<script src="{{ asset('js/admin.js') }}"></script>
<script>
document.addEventListener('DOMContentLoaded', function () {
    console.log('DOM content loaded');

    // Handle delete button click
    document.querySelectorAll('.btn-delete').forEach(function (button) {
        button.addEventListener('click', function () {
            console.log('Delete button clicked');

            const productId = this.getAttribute('data-id');
            console.log('Product ID:', productId);

            if (confirm('Are you sure you want to delete this product?')) {
                const form = document.getElementById('form-delete' + productId);
                form.submit();
            }
        });
    });
});

</script>

@endsection
