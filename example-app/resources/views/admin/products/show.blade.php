@extends('admin.layouts.app')
@section('title', 'Show Product')
@section('content')
    <div class="card">
        <h1>Show Product</h1>

        <div class="row">
            <div class="col-5">
                <p>Image</p>
                @if ($product->images && $product->images->isNotEmpty())
                    <img src="{{ asset('upload/' . $product->images->first()->url) }}" id="show-image" alt="">
                @else
                    <img src="{{ asset('upload/default.png') }}" id="show-image" alt="Default Image">
                @endif
            </div>

            <div class="col-4">
                <p>Name : {{ $product->name }}</p>
                <p>Price: {{ $product->price }}</p>
                <p>Sale: {{ $product->sale }}</p>

                <div class="form-group">
                    <p>Description</p>
                    <div class="row w-100 h-100">
                        {!! $product->description !!}
                    </div>
                </div>

                <div>
                    <p>Size</p>
                    @if ($product->details && $product->details->count() > 0)
                        @foreach ($product->details as $detail)
                            <p>Size: {{ $detail->size }} - quantity: {{ $detail->quantity }}</p>
                        @endforeach
                    @else
                        <p> Sản phẩm này chưa nhập size</p>
                    @endif
                </div>
            </div>

            <div class="col-3">
                <p>Category</p>
                @if ($product->categories)
                    @forelse ($product->categories as $item)
                        <p>{{ $item->name }}</p>
                    @empty
                        <p>No categories assigned</p>
                    @endforelse
                @else
                    <p>No categories assigned</p>
                @endif
            </div>
            <div class="mt-3">
            <a href="{{ route('admin.products.edit', ['product' => $product->id]) }}" class="btn btn-primary">Edit Product</a>
            <a href="{{ route('products.index') }}" class="btn btn-secondary">Back to Product List</a>
        </div>
        </div>
    </div>
@endsection

