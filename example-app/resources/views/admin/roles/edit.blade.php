@extends('admin.layouts.app')
@section('title', 'Edit Roles ' . $role->name)
@section('content')
    <div class="card">
        <h1>Edit role</h1>

        <div>
            <form id="editRoleForm" action="{{ route('roles.update', $role->id) }}" method="post">
                @csrf
                @method('PUT')

                <div class="input-group input-group-static mb-4">
                    <label>Name</label>
                    <input type="text" value="{{ old('name') ?? $role->name }}" name="name" class="form-control">
                    <span class="text-danger" id="name_error"></span> <!-- Error message placeholder -->
                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Display Name</label>
                    <input type="text" value="{{ old('display_name') ?? $role->display_name }}" name="display_name" class="form-control">
                    <span class="text-danger" id="display_name_error"></span> <!-- Error message placeholder -->
                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Group</label>
                    <select name="group" class="form-control">
                        <option value="system" {{ $role->group === 'system' ? 'selected' : '' }}>System</option>
                        <option value="user" {{ $role->group === 'user' ? 'selected' : '' }}>User</option>
                    </select>
                    <span class="text-danger" id="group_error"></span> <!-- Error message placeholder -->
                </div>

                <div class="form-group">
                    <label for="">Permission</label>
                    <div class="row">
                        @foreach ($permissions as $groupName => $permission)
                            <div class="col-5">
                                <h4>{{ $groupName }}</h4>
                                <div>
                                    @foreach ($permission as $item)
                                        <div class="form-check">
                                            <input class="form-check-input" name="permission_ids[]" type="checkbox" {{ $role->permissions->contains('name', $item->name) ? 'checked' : '' }} value="{{ $item->id }}">
                                            <label class="custom-control-label" for="customCheck1">{{ $item->display_name }}</label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <button type="submit" class="btn btn-submit btn-primary">Submit</button>
            </form>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#editRoleForm').on('submit', function(event) {
                event.preventDefault();

                $.ajax({
                    url: $(this).attr('action'),
                    type: 'POST',
                    data: $(this).serialize(),
                    success: function(response) {
                        alert('Role edited successfully.');
                        window.location.href = "{{ route('roles.index') }}";
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        // Handle error response
                        if (xhr.status == 422) {
                            var errors = xhr.responseJSON.errors;
                            $.each(errors, function(key, value) {
                                $('#' + key + '_error').html('<span class="text-danger">' + value[0] + '</span>');
                            });
                        } else if (xhr.status == 500) {
                            var errorMessage = xhr.responseJSON.message;
                            alert(errorMessage);
                        } else {
                            alert('An unexpected error occurred. Please try again.');
                        }
                    }
                });
            });
        });
    </script>
@endsection


