@extends('admin.layouts.app')
@section('title', 'Roles')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="card">
    @if (session('message'))
        <h1 class="text-primary">{{ session('message') }}</h1>
    @endif

    <h1>Role list</h1>
    <div>
            <a href="{{ route('roles.create') }}" class="btn btn-primary">Create</a>
            <form action="{{ route('roles.index') }}" method="GET">
                <div class="form-group">
                    <label for="search">Search:</label>
                    <input type="text" name="search" id="search" class="form-control" placeholder="Enter search ">
                </div>
                <button type="submit" class="btn btn-primary">Search</button>
            </form>
        </div>
    <div class="table-responsive">
        <table class="table table-bordered table-hover">
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Display Name</th>
                <th>Action</th>
            </tr>
            @foreach ($roles as $role)
                <tr id="role_{{ $role->id }}">
                    <td>{{ $role->id }}</td>
                    <td>{{ $role->name }}</td>
                    <td>{{ $role->display_name }}</td>
                    <td>
                        <div class="btn-group" role="group">
                            <a href="{{ route('roles.edit', $role->id) }}" class="btn btn-warning">Edit</a>
                            <button class="btn btn-delete btn-danger" data-id="{{ $role->id }}">Delete</button>
                        </div>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
    {{ $roles->appends(request()->except('page'))->links() }}
</div>
<div class="modal fade" id="deleteConfirmationModal" tabindex="-1" role="dialog" aria-labelledby="deleteConfirmationModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteConfirmationModalLabel">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Are you sure you want to delete this role?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="cancelDelete">Cancel</button>
                <button type="button" class="btn btn-danger" id="confirmDelete">Delete</button>
            </div>
        </div>
    </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js" integrity="sha384-BBtl+eGJRgqQAUMxJ7pMwbEyER4l1g+O15P+16Ep7Q9Q+zqX6gSbd85u4mG4QzX+" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<script>
$(document).ready(function () {
    // Handle delete button click
    $('.btn-delete').click(function () {
        var roleId = $(this).data('id');
        $('#confirmDelete').data('id', roleId); // Set roleId as data-id for confirmDelete button
        $('#deleteConfirmationModal').modal('show');
    });

    // Handle confirm delete
    $('#confirmDelete').click(function () {
        var roleId = $(this).data('id'); // Get roleId from data-id
        $.ajax({
            url: '/roles/' + roleId,
            type: 'DELETE',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $('#deleteConfirmationModal').modal('hide');
                $('#role_' + roleId).remove();
                alert('Role deleted successfully.');
            },
            error: function (xhr) {
                console.error(xhr.responseText);
                window.location.reload(); 
            }
        });
    });

    // Handle cancel delete
    $('#cancelDelete').click(function () {
        $('#deleteConfirmationModal').modal('hide');
        });
    });
</script>
@endsection
