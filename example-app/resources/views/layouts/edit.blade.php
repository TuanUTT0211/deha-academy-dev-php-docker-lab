<!-- resources/views/users/create.blade.php and resources/views/users/edit.blade.php -->
<div class="form-group">
    <label for="role_id">Role:</label>
    <select name="role_id" id="role_id" class="form-control" required>
        <option value="" disabled>Select a role</option>
        @foreach($roles as $r)
            <option value="{{ $r->id }}" {{ old('role_id', isset($user) ? $user->role_id : '') == $r->id ? 'selected' : '' }}>
                {{ $r->name }}
            </option>
        @endforeach
    </select>
</div>


<div class="form-group">
    <button type="button" class="btn btn-primary" id="saveUser">Save</button>
</div>

<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#saveUser').on('click', function() {
            saveUser();
        });

        function saveUser() {
            // Assuming you have the CSRF token meta tag in your HTML
            var csrfToken = $('meta[name="csrf-token"]').attr('content');

            // Get the form data
            var formData = {
                role_id: $('#role_id').val(),
                // Add other form fields as needed
            };

            // Make an AJAX request
            $.ajax({
                type: 'POST',
                url: '{{ route("users.store") }}', // Adjust the route accordingly
                data: formData,
                headers: {
                    'X-CSRF-TOKEN': csrfToken
                },
                success: function(data) {
                    // Handle success, e.g., show a success message
                    console.log('User saved successfully!');
                },
                error: function(error) {
                    // Handle errors, e.g., show an error message
                    console.error('Error saving user:', error);
                }
            });
        }
    });
</script>
