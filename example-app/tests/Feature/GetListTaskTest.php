<?php

namespace Tests\Feature;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Http\Response;
class GetListTaskTest extends TestCase
{

    public function getListTaskRoute(){
        return route('task.index');
    }
/** @test */
    public function user_can_get_all_task()
    {
        $task = Task::factory()->create();
        $response = $this->get($this->getListTaskRoute());

        $response->assertStatus(Response::HTTP_INTERNAL_SERVER_ERROR);
        $response->assertSee($task->name);
    }

}
