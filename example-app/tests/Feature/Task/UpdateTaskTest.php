<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Contracts\Auth\Authenticatable;
use Tests\TestCase;
use Illuminate\Http\Response;

class UpdateTaskTest extends TestCase
{
    use RefreshDatabase;

/** @test */
public function authenticated_user_can_update_task()
{
    // Create a user and authenticate
    $user = User::factory()->create();
    /** @var \Illuminate\Contracts\Auth\Authenticatable $user */
    $this->actingAs($user);

    // Create a task to be updated
    $task = Task::factory()->create();

    // New data for the task
    $newData = [
        'name' => 'Updated Task Name',
        'content' => 'Updated Task Content',
        // Add other fields as needed
    ];

    // Make a request to update the task
    $response = $this->put(route('task.update', ['id' => $task->id]), $newData);

    // Update the assertion to check for a 200 status code
    $response->assertStatus(200);

    // Reload the task from the database
    $updatedTask = Task::find($task->id);

    // Assert that the task has been updated with the new data
    $this->assertEquals($newData['name'], $updatedTask->name);
    $this->assertEquals($newData['content'], $updatedTask->content);
    // Add other assertions for additional fields
}


    /** @test */
    public function unauthenticated_user_cannot_update_task()
    {
        // Create a task to be updated
        $task = Task::factory()->create();

        // New data for the task
        $newData = [
            'name' => 'Updated Task Name',
            'content' => 'Updated Task Content',
            // Add other fields as needed
        ];

        // Make a request to update the task without authenticating
        $response = $this->put(route('task.update', ['id' => $task->id]), $newData);

        // Assert that the response redirects to the login page
        $response->assertRedirect('/login');

        // Reload the task from the database to ensure it has not been updated
        $updatedTask = Task::find($task->id);

        // Assert that the task data has not changed
        $this->assertEquals($task->name, $updatedTask->name);
        $this->assertEquals($task->content, $updatedTask->content);
        // Add other assertions for additional fields
    }
}
