<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Contracts\Auth\Authenticatable;

use Tests\TestCase;
use Illuminate\Http\Response;
class CreateNewTaskTest extends TestCase
{
    use RefreshDatabase;

/** @test */
public function authenticated_user_can_new_task()
{
    $user = User::factory()->create();
    /** @var \Illuminate\Contracts\Auth\Authenticatable $user */
    $this->actingAs($user);
    $task = Task::factory()->make()->toArray();
    $response = $this->post($this->getCreateTaskRoute(), $task);

    // Update the assertion to check for a 200 status code
    $response->assertStatus(200);
}


/** @test */
public function unauthenticated_user_can_not_create_task()
{
    $task = Task::factory()->make()->toArray();
    $response = $this->post($this->getCreateTaskRoute(), $task);
    $response->assertRedirect('/login');
}

/** @test */
public function authenticated_user_can_not_create_task_if_name_field_is_null()
{
    $user = User::factory()->create();
    /** @var \Illuminate\Contracts\Auth\Authenticatable $user */
    $this->actingAs($user);
    $task = Task::factory()->make(['name' => null])->toArray();
    $response = $this->post($this->getCreateTaskRoute(), $task);

    // Update the assertion to check for a 422 status code (Unprocessable Entity)
    $response->assertStatus(422);

    // Check if the error message is present in the response content
    $response->assertJsonValidationErrors(['name' => 'The name field is required.']);
}



 /** @test */
 public function authenticated_user_can_view_create_task_form(){
    /** @var \Illuminate\Contracts\Auth\Authenticatable $user */

    $user = User::factory()->create(); // Create and get a single user instance
    $this->actingAs($user); // Authenticate the user
    $respone = $this->get($this->getCreateTaskViewRoute());
    $respone -> assertViewIs('task.create');  
 }

/** @test */
public function authenticated_user_can_see_name_required_text_if_validate_error()
{
    $user = User::factory()->create();
    /** @var \Illuminate\Contracts\Auth\Authenticatable $user */
    $this->actingAs($user);
    $task = Task::factory()->make(['name' => null])->toArray();

    // Use the 'from' method to set the previous URL before making the request
    $response = $this->from($this->getCreateTaskViewRoute())->post($this->getCreateTaskRoute(), $task);

    // Update the assertion to check for a 422 status code (Unprocessable Entity)
    $response->assertStatus(422);

    // Check if the error message is present in the response content
    $response->assertJsonValidationErrors(['name' => 'The name field is required.']);
}


/** @test */
 public function unauthenticated_user_can_not_see_create_task_form_view(){
    $response = $this->get($this->getCreateTaskViewRoute());
    $response->assertRedirect('/login');
 }

    public function getCreateTaskRoute(){
        return route('task.store');
    }

    public function getCreateTaskViewRoute(){
        return route('task.create');
}
}
