<?php

namespace Tests\Feature\Posts;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;
class GetListPostTest extends TestCase
{
    /** @test */
    public function user_can_get_list_posts(){

        $postCount = Post::count();
        $response = $this->getJson(route('posts.index'));//tra ve api, goi phuong thuc get, function nhat la route

        $response->assertStatus(Response::HTTP_OK);//dung ham assert:la ham kiem tra du lieu dung hay khong, kiem tra trang thai dung la 200 ko

        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('data', fn (AssertableJson $json)=>
                $json->has('data')
                    // ->has('links')
                    ->has('meta', fn (AssertableJson $json)=>
                        $json->where('total',$postCount)
                            ->etc()
                )
        )
        ->has('message') //test du lieu tra ve co dung key ko, dung response tro den assert, dung arrow function, trong do co asserttableJson, check xem key co data va message ko
    );
    }
}
