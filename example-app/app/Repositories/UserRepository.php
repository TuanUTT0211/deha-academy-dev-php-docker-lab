<?php
namespace App\Repositories;

use App\Models\User;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;

class UserRepository extends BaseRepository
{
    public function model()
    {
        return User::class;
    }

    /**
     * @param string|null $search
     * @param int $limit
     * @return mixed
     */
    public function getWithPaginate(?string $search = null, int $limit = 5): mixed
    {
        $query = $this->model->latest('id');

        if ($search !== null) {
            $query->where(function (Builder $builder) use ($search) {
                $builder->where('name', 'like', "%$search%")
                    ->orWhere('email', 'like', "%$search%")
                    ->orWhere('phone', 'like', "%$search%");
            });
        }

        return $query->paginate($limit);
    }
}
