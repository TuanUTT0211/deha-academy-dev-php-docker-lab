<?php
namespace App\Repositories;

use App\Models\Role;
use App\Repositories\BaseRepository;
use App\Models\Product;

class ProductRepository extends BaseRepository
{

    public function model()
    {
        return Product::class;
    }

    /**
     * @param $limit
     * @return mixed
     */
    public function getWithPaginate($limit = 5): mixed
    {
        return $this->model->latest('id')->paginate($limit);
    }
    public function getAllProducts()
    {
        // Assuming you have an Eloquent query to retrieve all products from the database
        return Product::all();
    }

}
