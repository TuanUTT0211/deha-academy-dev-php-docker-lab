<?php

// app/Repositories/PermissionRepository.php
namespace App\Repositories;

use App\Models\Permission;
use Spatie\Permission\Models\Permission as ModelsPermission;

class PermissionRepository extends BaseRepository
{
    public function model(): string
    {
        return ModelsPermission::class;
    }
}
