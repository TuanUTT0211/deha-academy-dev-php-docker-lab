<?php
namespace App\Repositories;

use App\Models\Role;
use Illuminate\Http\Request;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Builder;

class RoleRepository extends BaseRepository
{

    public function model()
    {
        return Role::class;
    }

    /**
     * @param string|null $search
     * @param int $limit
     * @return mixed
     */
    public function getWithPaginate(?string $search = null, int $limit = 5): mixed
    {
        $query = $this->model->latest('id');

        if ($search !== null) {
            $query->where(function (Builder $builder) use ($search) {
                $builder->where('name', 'like', "%$search%")
                    ->orWhere('display_name', 'like', "%$search%");
            });
        }

        return $query->paginate($limit);
    }
    public function getRoleByName($name)
    {
        try {
            return $this->model->where('name', $name)->firstOrFail();
        } catch (ModelNotFoundException $exception) {
            return null; // Return null if no role with the specified name is found
        }
    } 
}
