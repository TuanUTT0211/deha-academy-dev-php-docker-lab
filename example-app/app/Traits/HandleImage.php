<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as Image;

trait HandleImage
{
    protected string $path = 'upload/';

    /**
     * @param $request
     * @return mixed
     */
    public function verify($request): mixed
    {
        return $request->has('image');
    }

    /**
     * @param $request
     * @return string|void
     */
    public function saveImage($request)
    {
        if ($this->verify($request)) {
            $file = $request->file('image');
            $name = time() . $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();

            // Use Intervention Image to resize the image
            $image = Image::make($file)->resize(300, 300)->encode();

            // Store the resized image
            Storage::put($this->path . $name, $image->__toString());

            return $name;
        }
    }

    /**
     * @param $request
     * @param $currentImage
     * @return mixed|string|null
     */
    public function updateImage($request, $currentImage): mixed
    {
        if ($this->verify($request)) {
            $this->deleteImage($currentImage);
            return $this->saveImage($request);
        }

        return $currentImage;
    }

    /**
     * @param $imageName
     * @return void
     */
    public function deleteImage($imageName): void
    {
        if ($imageName && Storage::exists($this->path . $imageName)) {
            Storage::delete($this->path . $imageName);
        }
    }
}
