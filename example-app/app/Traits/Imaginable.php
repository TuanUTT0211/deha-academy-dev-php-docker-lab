<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Casts\Attribute;

trait Imaginable
{
    // Define your property
    protected string $path = 'upload/';

    public function images()
    {
        return $this->morphMany(\App\Models\Image::class, 'imageable');
    }

    /**
     * @return string|null
     */
    public function getImagePathAttribute()
    {
        return $this->images->first() ? asset($this->imageShowPath . $this->images->first()->url) : null;
    }

    /**
     * @param $imageUrl
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function syncImage($imageUrl)
    {
        $this->destroyImage();
        return $this->images()->create(['url' => $imageUrl]);
    }

    /**
     * @return int
     */
    public function destroyImage(): int
    {
        return $this->images()->delete();
    }
}
