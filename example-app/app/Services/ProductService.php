<?php

namespace App\Services;

use App\Repositories\CategoryRepository;
use App\Repositories\ProductRepository;
use App\Models\Product;
use App\Traits\HandleImage;
use App\Traits\Imaginable;
use Illuminate\Http\Request;

class ProductService
{
    protected ProductRepository $productRepository;
    use HandleImage, Imaginable;

    /**
     * @param ProductRepository $productRepository
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @param $request
     * @return mixed
     */

    public function store($request): mixed
    {
        $dataCreate = $request->except('sizes');
        $sizes = $request->sizes ? json_decode($request->sizes) : [];
        $product = $this->productRepository->create($dataCreate);
        $dataCreate['image'] = $this->saveImage($request);
        return $product;
    }

    /**
     * @param $request
     * @param $id
     * @return mixed
     */
    public function update($request, $id): mixed
    {
        $product = $this->findOrFail($id);
        $dataUpdate = $request->except('sizes');
        $sizes = $request->sizes ? json_decode($request->sizes) : [];
        
        $currentImage = optional($product->images)->first()?->url;
        
        $dataUpdate['image'] = $this->updateImage($request, $currentImage);
        
        $product->update($dataUpdate);
        
        return $product;
    }
    

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id): mixed
    {
        $product = $this->findOrFail($id);
        $currentImage = optional($product->images)->first()?->url;

        // Delete the product and its associated image
        $product->delete();
        $this->deleteImage($currentImage);

        return $product;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findOrFail($id): mixed
    {
        return $this->productRepository->findOrFail($id);
    }

    public function getWithPaginate()
    {
        $products = $this->productRepository->getWithPaginate();

        return Product::with('imageData')->paginate(10);
    }

    protected function formatImagePath($products)
    {
        foreach ($products as $product) {
            $product->image_path = $this->generateImagePath($product->image_path);
        }

        return $products;
    }
    protected function generateImagePath($imagePath)
    {
        return asset('upload/' . $imagePath);
    }
    /**
     * @param mixed $product
     * @param mixed $dataCreate
     * @param mixed $sizes
     * @return null|Product
     */
    public function updateDetail(Product $product, mixed $dataCreate, mixed $sizes, $id): Product|null
    {
        $currentImage = optional($product->image_path)->first()?->url;

        // Delete the product and its associated image
        $productId = $product->id; // Save the product ID before deleting
        $product->delete();
        $this->deleteImage($currentImage);

        // Retrieve the product using the saved ID
        $updatedProduct = $this->findOrFail($productId);

        return $updatedProduct;
    }
}
