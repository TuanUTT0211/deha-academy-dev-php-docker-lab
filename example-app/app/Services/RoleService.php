<?php

namespace App\Services;

use App\Repositories\BaseRepository;
use App\Repositories\RoleRepository;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Eloquent\Model;   
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class RoleService
{
    protected RoleRepository $roleRepository;

    /**
     * @param RoleRepository $roleRepository
     */
    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function create($request): mixed
    {
        $dataCreate = $request->all();
        $dataCreate['guard_name'] = 'web';
        $role = $this->roleRepository->create($dataCreate);
        $role->permission()->attach($dataCreate['permission_ids']);
        return $role;
    }
    

    /**
     * Update the role.
     *
     * @param $request
     * @param $id
     * @return Model
     * @throws ValidationException
     */
    public function update($request, $id): Model
    {
        $role = $this->roleRepository->findOrFail($id);
        $dataUpdate = $request->all();

        unset($dataUpdate['permission_ids']);

        $existingRole = $this->roleRepository->getRoleByName($dataUpdate['name']);

        if ($existingRole && $existingRole->id !== $role->id) {
            throw ValidationException::withMessages(['name' => 'Role with this name already exists.'])->redirectTo(route('roles.edit', $id));
        }
        $role->update($dataUpdate);

        return $role;
    }
    
    /**
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function findOrFail($id, array $columns = ['*']): mixed
    {
        return $this->roleRepository->findOrFail($id, $columns);
    }

    /**
     * @param $id
     * @return int
     */
    public function delete($id): int
    {
        return $this->roleRepository->delete($id);
    }

    /**
     * @return mixed
     */
    public function getWithPaginate(?string $search = null): mixed
    {
        return $this->roleRepository->getWithPaginate($search);
    }

    /**
     * @return mixed
     */
    public function getWithGroup(): mixed
    {
        return $this->roleRepository->all()->groupBy('group');
    }
}
