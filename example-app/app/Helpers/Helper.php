<?php
namespace App\Helpers;

use Illuminate\Support\Facades\File;

class Helper
{
    public static function requireRoute(): void
    {
        foreach (File::allFiles(base_path('routes')) as $route_file) {
            (basename($route_file) === 'web.php') ?: (require $route_file);
        }
    }

    public static function getGender($gender): string
    {
        if (is_null($gender)) {
            return '?';
        }
        if ($gender == 1) {
            return 'Man';
        }
        return 'Women';
    }

    public static function convertPhone($phone): string
    {
        if (!empty($phone) && preg_match('/^(\d{4})(\d{3})(\d{3})$/', $phone, $matches)) {
            return $matches[1].'-'.$matches[2].'-'.$matches[3];
        }

        return '?';
    }

    public static function getStatus($status): string
    {
        if ($status) {
            return '<span class="badge badge-success">Activated</span>';
        }
        return '<span class="badge badge-secondary">Inactivated</span>';
    }
}
