<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\DB;
class ProductController extends Controller
{

    protected $product;
    public function __construct(Product $product)
    {
        $this->product = $product;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $category_id)
    {
        // Fetch or generate your categories data
        $categories = Category::all(); // Replace this with your actual logic to get categories
    
        $products = $this->product->getBy($request->all(), $category_id);
    
        return view('client.products.index', compact('products', 'categories'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = $this->product->with('details')->findOrFail($id);
    
        return view('client.products.detail', compact('product'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
        /**
     * Attach categories to the specified product.
     *
     * @param  int  $product_id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function attachCategories($product_id, Request $request)
    {
        $product = Product::findOrFail($product_id);
        
        // Attach categories to the product using sync()
        $product->categories()->sync($request->input('category_ids', []));

        return redirect()->back()->with('success', 'Categories attached successfully.');
    }
    public function search(Request $request)
    {
        $search = $request->input('search');
        
        $products = $this->product->where('name', 'like', "%$search%")
                                  ->orWhere('description', 'like', "%$search%")
                                  ->get();
        
        return view('client.products.search', compact('products', 'search'));
    }
}
