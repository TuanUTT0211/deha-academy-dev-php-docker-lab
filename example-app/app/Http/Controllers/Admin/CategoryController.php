<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Categories\CreateCategoryRequest;
use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    protected $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function index()
    {
        $categories = $this->category->latest('id')->paginate(3);
        return view('admin.categories.index', compact('categories'));
    }

    public function create()
    {
        $parentCategories =  $this->category->getParents();
        return view('admin.categories.create', compact('parentCategories'));
    }

    public function store(CreateCategoryRequest $request)
    {
        $dataCreate = $request->all();
        $category = $this->category->create($dataCreate);
        return redirect()->route('categories.index')->with(['message' => 'Create new category: ' . $category->name . ' success']);
    }

    public function edit($id)
    {
        $category = $this->category->with('childrens')->findOrFail($id);
        $parentCategories = $this->category->whereNull('parent_id')->get();
    
        // If a parent category is selected, include it in the list
        $selectedParentId = $category->parent_id;
    
        return view('admin.categories.edit', compact('category', 'parentCategories'));
    }
    

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string',
            'parent_id' => 'nullable|exists:categories,id', // Validate that the parent_id exists in the categories table
        ]);

        $dataUpdate = $request->all();
        $category = $this->category->findOrFail($id);

        // Ensure parent_id is set to null if not provided
        $dataUpdate['parent_id'] = $request->input('parent_id');

        $category->update($dataUpdate);
        return redirect()->route('categories.index')->with(['message' => 'Update category: ' . $category->name . ' success']);
    }

    public function destroy($id)
    {
        $category = $this->category->findOrFail($id);
        $category->delete();
        return redirect()->route('categories.index')->with(['message' => 'Delete category: ' . $category->name . ' success']);
    }
}
