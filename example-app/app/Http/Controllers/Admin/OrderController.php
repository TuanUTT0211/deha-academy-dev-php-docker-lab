<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use Illuminate\Http\Response;

class OrderController extends Controller
{
    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function index()
    {
        // Assuming getWithPaginateBy is a method in your Order model
        $orders = $this->order->getWithPaginateBy(auth()->user()->id);
        
        // Make sure $orders is paginated
        if (!$orders instanceof \Illuminate\Pagination\LengthAwarePaginator) {
            abort(500, 'Invalid pagination result');
        }

        return view('admin.orders.index', compact('orders'));
    }

    public function updateStatus(Request $request, $id)
    {
        $order = $this->order->findOrFail($id);

        // Validate the status if needed before updating
        $request->validate([
            'status' => 'required|in:pending,processed,shipped,delivered,canceled',
        ]);

        $order->update(['status' => $request->status]);

        return response()->json([
            'message' => 'success'
        ], Response::HTTP_OK);
    }
}
