<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can edit their own profile.
     *
     * @param  \App\Models\User  $loggedInUser
     * @param  \App\Models\User  $profileUser
     * @return bool
     */
    public function edit(User $loggedInUser, User $profileUser)
    {
        return $loggedInUser->id === $profileUser->id;
    }
    public function update(User $user)
    {
        return $user->id == auth()->user()->id;
    }
}
