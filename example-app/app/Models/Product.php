<?php

namespace App\Models;

use App\Traits\HandleImage;
use App\Traits\Imaginable;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Product extends Model
{
    use HasFactory, HandleImage, Imaginable;


    protected $fillable = [
        'name',
        'description',
        'sale',
        'product_id',
        'image_path',
        'price'
    ];

    public function details()
    {
        return $this->hasMany(ProductDetail::class);
    }

    public function categories() : BelongsToMany
    {
        return $this->belongsToMany(Category::class);
    }

    public function assignCategory($categoryIds)
    {
        return $this->categories()->sync($categoryIds);
    }

    public function imageData()
    {
        return $this->hasOne(Image::class, 'image_path');
    }

    public function getBy($dataSearch, $categoryId)
    {
        return $this->whereHas('categories', fn($q) => $q->where('category_id', $categoryId))->paginate(10);
    }


    public function salePrice(): float
    {
        return $this->attributes['sale']
            ? $this->attributes['price'] - ($this->attributes['sale'] * 0.01 * $this->attributes['price'])
            : 0;
    }
    // Inside the Product model (App\Models\Product)
    public function productDetails()
    {
        return $this->hasMany('App\Models\Detail');
    }    
    public function categoriesProduct() : BelongsToMany
    {
        return $this->belongsToMany('App\Models\Category');
    }
    
}
