<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role as ModelsRole;
use Illuminate\Database\Eloquent\Casts\Attribute;
use App\Traits\HandleImage;
use App\Models\Permission;
class Role extends ModelsRole
{
    use HasFactory,HandleImage;

    protected $fillable = [
        'role_id',
        'name',
        'display_name',
        'group',
        'guard_name'
    ];
    public function details()
    {
        return $this->hasOne('App\Models\Detail');
    }
    public function categories()
    {
        return $this->hasOne('App\Models\Detail');
    }
    public function permission()
    {
        return $this->belongsToMany('App\Models\Permission');
    }
}
