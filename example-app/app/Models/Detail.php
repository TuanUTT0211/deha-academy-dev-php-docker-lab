<?php

namespace App\Models;
use App\Models\Role;
use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    // Specify the table associated with the model
    protected $table = 'details';

    // Define the fillable columns
    protected $fillable = [
        'column1',
        'column2',
        // Add other columns as needed
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }
    
}
