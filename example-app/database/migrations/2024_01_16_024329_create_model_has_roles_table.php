<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModelHasRolesTable extends Migration
{
    public function up()
    {
        Schema::create('model_has_roles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('model_id');
            $table->string('model_type');
            $table->unsignedBigInteger('role_id');
            // Add any additional columns you may need

            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            // You may need to adjust the foreign key reference based on your 'roles' table

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('model_has_roles');
    }
}
