<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ProductOrder>
 */
class ProductOrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'product_size' => $this->faker->randomElement(['Small', 'Medium', 'Large']),
            'product_color' => $this->faker->colorName,
            'product_quantity' => $this->faker->numberBetween(1, 10),
            'product_price' => $this->faker->randomFloat(2, 10, 100),
            'order_id' => \App\Models\Order::factory(),
            'user_id' => \App\Models\User::factory(),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
