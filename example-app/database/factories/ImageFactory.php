<?php

namespace Database\Factories;

use App\Models\Image;
use Illuminate\Database\Eloquent\Factories\Factory;

class ImageFactory extends Factory
{
    public function definition()
    {
        return [
            'url' => $this->faker->imageUrl(),
            'imageable_id' => $this->faker->numberBetween(1, 100), // Set your desired range
            'imageable_type' => $this->faker->word, // Set your desired value or leave it as null
        ];
    }
}
