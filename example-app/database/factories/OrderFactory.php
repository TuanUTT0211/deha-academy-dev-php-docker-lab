<?php

namespace Database\Factories;

use App\Models\Order;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'status' => $this->faker->randomElement(['pending', 'processing', 'completed']),
            'total' => $this->faker->randomFloat(2, 10, 100),
            'ship' => $this->faker->randomFloat(2, 1, 10),
            'customer_name' => $this->faker->name,
            'customer_email' => $this->faker->email,
            'customer_phone' => $this->faker->phoneNumber,
            'customer_address' => $this->faker->address,
            'note' => $this->faker->text,
            'user_id' => User::factory()->create()->id, // Assuming there's a User model and factory
        ];
    }
}

