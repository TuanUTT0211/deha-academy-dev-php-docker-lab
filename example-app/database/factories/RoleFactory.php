<?php

namespace Database\Factories;

use App\Models\Role;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Testing\WithFaker;

class RoleFactory extends Factory
{
    protected $model = Role::class;

    /**
     *
     * @return array
     */
    public function definition()
    {
        $roleIds = Role::pluck('role_id')->toArray();

        return [
            'name' => $this->faker->name(),
            'display_name' => $this->faker->name(),
            'group' => 'sys',
            'guard_name' => 'web',
            'role_id' => function () use ($roleIds) {
                $roleId = $this->faker->randomNumber();

                while (in_array($roleId, $roleIds)) {
                    $roleId = $this->faker->randomNumber();
                }

                return $roleId;
            },
            'updated_at' => now(),
            'created_at' => now(),
        ];
    }
}
